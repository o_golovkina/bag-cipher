﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagPacking
{
    class Program
    {
        static char GetSymbol(byte code)
        {
            Encoding ASCII_Ext = Encoding.GetEncoding(1251);
            return ASCII_Ext.GetChars(new byte[] { code })[0];
        }

        static byte GetCode(char symbol)
        {
            Encoding ASCII_Ext = Encoding.GetEncoding(1251);
            return ASCII_Ext.GetBytes(new char[] { symbol })[0];
        }

        static List<int> EncryptMessage(string message, List<int> openedKey)
        {
            List<int> encryptedMessage = new List<int>();

            foreach (var symbol in message)
            {
                string code = Convert.ToString(GetCode(symbol), 2);
                encryptedMessage.Add(GetCipher(code, openedKey));
            }
            return encryptedMessage;
        }

        static int GCD(int a, int b)
        {
            if (a == 0)
            {
                return b;
            }
            else
            {
                var min = Min(a, b);
                var max = Max(a, b);
                //вызываем метод с новыми аргументами
                return GCD(max - min, min);
            }
        }

        static int Min(int x, int y)
        {
            return x < y ? x : y;
        }

        static int Max(int x, int y)
        {
            return x > y ? x : y;
        }

        static int SearchN_1(int m, int n)
        {
            int x = 0;
            int n_1 = (x * m + 1) / n;

            while ((x * m + 1) % n != 0)
            {
                Debug.WriteLine(n_1);
                Debug.WriteLine(n_1);

                x++;
                n_1 = (x * m + 1) / n;
            }

            return n_1;
        }

        static string DecryptMessage(List<int> message, int m, int n, List<int> closedKey)
        {
            string decryptedMessage = string.Empty;
            int n_1 = SearchN_1(m, n);

            foreach (var code in message)
            {
                string d_code = string.Empty;
                int r = (code * n_1) % m;

                var minWay = GetMinWay(closedKey, r);

                foreach (var key in closedKey)
                {
                    if (minWay.Contains(key))
                        d_code += "1";
                    else
                        d_code += "0";
                }

                d_code = d_code.PadRight(8, '0');

                Debug.WriteLine(d_code);

                int s_code = Convert.ToInt32(d_code, 2);

                decryptedMessage += GetSymbol((byte)s_code);
            }

            return decryptedMessage;
        }

        static int GetCipher(string code, List<int> openedKey)
        {
            int res = 0;

            for (int i = 0; i < code.Length; i++)
                res += Int32.Parse(code[i].ToString()) * openedKey[i];

            return res;
        }

        public static int[] BinaryRepresent(int num, int length) // length - количество единиц в самом большом числе
        {
            var resultArray = new int[length];
            for (int i = length - 1; i >= 0; --i)
            {
                resultArray[i] = num % 2;
                num /= 2;
            }
            return resultArray;
        }

        static List<int> GetMinWay(List<int> numList, int key)
        {
            var actualArray = (from item in numList where item <= key select item).ToArray(); // Отрезаем заведомо лишние элемет
            // arr[i] <= n иначе не берём
            List<string> shortestWays = null;

            if (actualArray.Length == 0)
                return null;

            int countOfCases = 1;
            for (int i = 1; i < actualArray.Length; ++i)
                countOfCases *= 2 + 1; //Здесь мы получаем количество возможных вариантов 
                                       //( в бинарном представлении эти числа будут состоять из одних единиц)
                                       // В данном случае 1 - элемент включаем в сумму, 0 - нет.
                                       // 001 010 100 110 011 и т.д. думаю, принцип понятен.

            var selectionsList = new List<string>();
            shortestWays = new List<string>();

            int currentMinLengtWay = 0;
            for (int i = 1, oneCount = 0; i <= countOfCases; ++i)
            {
                int tempResult = 0; //Промежуточный результат (числа ведь только натуральные, ноль недостижим)
                var sb = new StringBuilder(); // Строковое представление пути
                var selection = BinaryRepresent(i, actualArray.Length); // Получаем выборку из нулей и единиц (берём только числа, соотв. единице)
                for (int j = 0; j < selection.Length; ++j) // Начинаем проверять, где единички, те элементы и будем складывать.
                    if (selection[j] == 1)
                    {
                        tempResult += actualArray[j];
                        if (sb.Length != 0) // Если не первое вхождение, то добавляем знак плюса
                            sb.Append(";");
                        sb.Append(actualArray[j].ToString());
                        oneCount++; // ДЛя кратчайшего пути, показывает количество элементов в пути. (количество единиц в массиве)
                    }

                var result = sb.ToString(); // преобразуем объект стрингБилдера в строку
                if (tempResult == key && !selectionsList.Contains(result)) // Если он нам подошёл, то формируем из него строку
                {                                                           // И если такого набора там ещё нет.
                    selectionsList.Add(result);

                    if (shortestWays.Count == 0)
                    {
                        shortestWays.Add(result); // если список кратчайших путей пуст, добавляем имеющийся
                        currentMinLengtWay = oneCount;
                    }
                    else
                    {

                        if (oneCount == currentMinLengtWay) // если текущая длина равна той минимальной длние, которая была до этого момента
                            shortestWays.Add(result);

                        if (oneCount < currentMinLengtWay) // если меньше, то чистим список
                        {
                            shortestWays.Clear();
                            shortestWays.Add(result);
                            currentMinLengtWay = oneCount;
                        }
                    }

                }
            }
            return shortestWays.First().Split(';').Select(x => Int32.Parse(x)).ToList();
        }


        static void Main(string[] args)
        {
            //m = 420 n = 31
            int m = 0, n = 0;
           
            Console.Write("m = ");
            m = Int32.Parse(Console.ReadLine());

            Console.Write("n = ");
            n = Int32.Parse(Console.ReadLine());

            if (GCD(m, n) != 1)
            {
                Console.WriteLine($"m и n не взаимно простые числа");
                Console.Read();
                return;
            }

            Random random = new Random();

            List<int> closedKey = new List<int>();
            List<int> openedKey = new List<int>();

            Console.WriteLine($"\nВведите закрытый ключ: ");
            //2 3 6 13 27 52 105 210
            do
            {
                bool ok = true;
                for (int i = 0; i < 8; i++)
                {
                    if (!ok)
                        i--;

                    Console.Write($"[{i}]: ");

                    int k = int.Parse(Console.ReadLine());

                    if (closedKey.Count == 0)
                    {
                        closedKey.Add(k);
                        ok = true;
                    }
                    else if (closedKey.Sum() < k)
                    {
                        ok = true;
                        closedKey.Add(k);
                    }
                    else
                    {
                        Console.WriteLine($"Новое значение не должно превышать сумме предыдущих значений: {closedKey.Sum()}");
                        ok = false;
                        continue;
                    }
                }

                bool Normal = !(m < closedKey.Sum());

                if (!Normal)
                    Console.WriteLine("Суммарное значение ключа не должно превышать m.");
            } while (m < closedKey.Sum());


            openedKey.AddRange(from x in closedKey select (x * n) % m);

            int diff = 8 - openedKey.Count;
            
            List<int> temp = new List<int>();
            
            temp.AddRange(openedKey);

            if (diff > 0)
                openedKey.AddRange(temp.Take(diff));
            else
                openedKey.RemoveRange(8, Math.Abs(diff));

            Console.WriteLine("\nЗакрытый ключ:");

            foreach (var key in closedKey)
                Console.Write($"{key} ");

            Console.WriteLine("\nОткрытый ключ:");

            foreach (var key in openedKey)
                Console.Write($"{key} ");

            Console.Write("\n\nВведите сообщение: ");
            string message = Console.ReadLine();

            List<int> encryptedMessage = EncryptMessage(message, openedKey);

            Console.WriteLine("\nЗашифрованное сообщение: ");
            foreach (int c in encryptedMessage)
                Console.Write($"{c} ");

            Console.WriteLine($"\nРасшифрованное сообщение: {DecryptMessage(encryptedMessage, m, n, closedKey)}");

            Console.ReadLine();
        }
    }
}
